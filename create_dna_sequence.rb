#!/usr/bin/env ruby
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This script creates a random DNA sequence, with options for creating repeated
#   regions. This might be useful for testing other scripts.

$fasta_wrap_length = 80

require 'optparse'

options = {}
OptionParser.new do |opts|
  opts.banner='This script creates a random DNA sequence
Usage:'
  opts.on_tail('-h', '--help', 'Show this message') do
    puts opts; exit
  end
  opts.on('-o', '--output FASTA_FILE.fa', 'Required. Output will be written '\
      'to FASTA_FILE.fa') do |o|
    options[:output] = o
  end
  opts.on('-l', '--length INTEGER', 'Required. The total length of the DNA '\
      'sequence is required.') do |l|
    options[:length] = l.to_i
  end
  opts.on('-r', '--repeats PATTERN', 'Optional. Define repeats. e.g. for '\
      '5 x 6 bp repeats + 7 x 8 bp repeats, PATTERN = 5x6,7x8') do |r|
    options[:repeats] = r
  end
  opts.on('-v', '--[no-]verbose', 'Run verbosely') do |v|
    options[:verbose] = v
  end
end.parse!

# Check and parse options.
raise OptionParser::MissingArgument if options[:output].nil?
raise OptionParser::MissingArgument if options[:length].nil?
raise OptionParser::InvalidArgument if options[:length] < 1
if options[:repeats]
  repeats_requested = []
  options[:repeats].split(',').each do |n_l|
    n_l_array = n_l.split('x')
    repeats_requested << {n:n_l_array.first.to_i, l: n_l_array.last.to_i}
  end
end

################################################################################
### Define DNA object.
class DNA
  def initialize
    @seq = ''
  end

  # Split an integer into integers of random lengths.
  def split_num(total_length, segments)
    try_again = true
    while try_again do
      splits = []
      segments.times {splits << rand}
      splits_total = 0.to_f
      splits.each {|segment| splits_total += segment}
      splits.collect! {|segment| (segment / splits_total * total_length).round}
      splits_total = 0
      splits.each {|segment| splits_total += segment}
      try_again = false if splits_total == total_length # Check for rounding.
    end
    splits
  end

  # Return a random sequence of a given length.
  def seq_fragment(length)
    fragment = ''
    length.times do
      nt_index = rand(4)
      case nt_index
        when 0; fragment << 'a'
        when 1; fragment << 't'
        when 2; fragment << 'c'
        when 3; fragment << 'g'
        else abort('Error in random number.')
      end
    end
    fragment
  end

  def create_seq_with_repeats(total_length, repeats, verbose)
    total_repeat_length = 0
    repeats.each do |repeat|
      total_repeat_length += (repeat[:n] * repeat[:l])
    end
    abort("ERROR: total_length < total_repeat_length (#{total_length} < "\
        "#{total_repeat_length})") if total_length < total_repeat_length
    repeat_count = repeats.length
    # Find random locations for repeats.
    non_repeat_length = total_length - total_repeat_length
    non_repeat_segments_lengths = split_num(non_repeat_length, repeat_count + 1)
    # Create sequence.
    @seq << seq_fragment(non_repeat_segments_lengths.shift)
    while repeats.length > 0 do
      # Write repeat.
      repeat_index = rand(repeats.length)
      single_seq = seq_fragment(repeats[repeat_index][:l])
      puts("Creating #{repeats[repeat_index][:n]} repeats of "\
          "'#{single_seq}'.") if verbose
      repeats[repeat_index][:n].times {@seq << single_seq}
      repeats.delete_at(repeat_index)
      # Write non-repeat.
      @seq << seq_fragment(non_repeat_segments_lengths.shift)
    end
  end

  def create_seq_no_repeats(total_length)
    @seq << seq_fragment(total_length)
  end

  def write_to_file(output_path, repeat_pattern)
    File.open(output_path, 'w') do |output_file|
      # Write header.
      output_file.print('>random_sequence')
      if repeat_pattern
        output_file.puts("_with_repeats_#{repeat_pattern}")
      else
        output_file.puts
      end
      while @seq.length > 0
        output_file.puts(@seq.slice!(0..($fasta_wrap_length-1)))
      end
    end
  end
end

################################################################################
### Run script proper.
new_dna = DNA.new
if repeats_requested
  new_dna.create_seq_with_repeats(options[:length], repeats_requested, options[:verbose])
else
  new_dna.create_seq_no_repeats(options[:length])
end
new_dna.write_to_file(options[:output], options[:repeats])