#!/usr/bin/env ruby
#
# Copyright 2017 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Output a fasta file containing the sequence up to n nucleotides upstream of
#   the ATG for all genes provided in a given gff file, using chromosomal
#   sequences from another provided fasta file.
# Useful for motif identification in (e.g.) MEME-ChIP.

require 'optparse'
$options = {}
OptionParser.new do |opts|
  opts.banner='This script reads a gff and fasta file, and outputs a fasta ' \
              'file with the sequence n nucleotides upstream of the ' \
              "ATG.\nUsage:"
  opts.on_tail('-h', '--help', 'Show this message') do
    puts opts; exit
  end
  opts.on('-g', '--ref_gff GFF_FILE',
          'Reference GFF_FILE (e.g. from eupathdb) is required.') do |g|
    $options[:refgff_path] = g
  end
  opts.on('-f', '--ref_fasta FASTA_FILE',
          'Reference FASTA_FILE is required.') do |f|
    $options[:reffasta_path] = f
  end
  opts.on('-o', '--output OUTPUT_FILE',
          'OUTPUT_FILE is required.') do |o|
    $options[:output_path] = o
  end
  opts.on('-n', '--nucleotides WIDTH', OptionParser::DecimalInteger,
          'Number of nucleotides upstream to include. Defaults to 1000.' \
         ) do |n|
    $options[:nucleotides] = n
  end
end.parse!

# Mandatory "options"
raise OptionParser::MissingArgument if $options[:refgff_path].nil? ||
    $options[:reffasta_path].nil? ||
    $options[:output_path].nil?
$options[:nucleotides] = 1000 if !$options[:nucleotides]

# Import all coords from GFF
cds = {}
File.open($options[:refgff_path]).each do |line|
  splitline = line.split("\t")
  if splitline[2] == 'CDS'
    # If they are AS, e.g. `PBANKA_0111300.1-1` and `PBANKA_0111300.2-1`, just use the earliest ATG.
    matchdata = /Parent=rna_(PBANKA_[0-9A-Z]*)(\.[12])?-1(;|$)/.match(splitline[8])
    if matchdata
      cds[matchdata[1]] ||= {} # .to_sym?
      cds[matchdata[1]][:coords] ||= []
      cds[matchdata[1]][:coords].push(splitline[3].to_i)
      cds[matchdata[1]][:coords].push(splitline[4].to_i)
      cds[matchdata[1]][:strand] = splitline[6]
      cds[matchdata[1]][:chr] = splitline[0]
    else
      abort('ERROR: the following line does not contain a gene_id in the ' \
          "expected format\n#{line}")
    end
  end
end

# Import all sequences from fasta
seq = {}
current_chr = ""
File.open($options[:reffasta_path]).each do |line|
  line = line.chomp
  matchdata = /^>([^ ]*)/.match(line)
  if matchdata
    current_chr = matchdata[1]
    seq[current_chr] = "" # restart, even if existing
  else
    if /[^atgcATGC]/.match(line)
      abort('ERROR: the following fasta line contains a non-ATGCatgc ' \
            "character\n#{line}")
    else
      seq[current_chr] << line
    end
  end
end

cds.each do |gene, data|
  # Just keep min and max coords
  data[:coords] = data[:coords].minmax

  # Get coords $options[:nucleotides] upstream
  if data[:strand] == '+'
    data[:coords] = [(data[:coords][0] - $options[:nucleotides] - 1), (data[:coords][0] - 1 - 1)]
  elsif data[:strand] == '-'
    data[:coords] = [(data[:coords][1] + 1 - 1), (data[:coords][1] + $options[:nucleotides] - 1)]
  else
    abort("ERROR: gene #{gene}'s strand is odd: #{data[:strand]}")
  end

  # MEME-ChIP: "The primary sequences should all be the same length… The control sequences should all be the same length as the primary sequences."
  # Hence, remove gene if < $options[:nucleotides].
  if (data[:coords][0] < 0) || (data[:coords][1] > (seq[data[:chr]].length - 1))
    cds.delete(gene)
  end

  # Get sequence for each gene
  if data[:strand] == '+'
    data[:seq] = seq[data[:chr]][(data[:coords][0])..data[:coords][1]]
  else # negative strand, so reverse complement
    data[:seq] = seq[data[:chr]][(data[:coords][0])..data[:coords][1]].
      gsub(/./,
        'a' => 't',
        't' => 'a',
        'g' => 'c',
        'c' => 'g',
        'A' => 'T',
        'T' => 'A',
        'G' => 'C',
        'C' => 'G',
          ).reverse
  end
end

# Write to file
File.open($options[:output_path], 'w') do |output_file|
  cds.each do |gene, data|
    output_file.puts(">#{gene}")
    output_file.puts(data[:seq])
  end
end
