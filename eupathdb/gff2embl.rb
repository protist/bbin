#!/usr/bin/env ruby
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This script reads a GFF file from EuPathDB and converts it into an EMBL file
#   as per ftp://ftp.ebi.ac.uk/pub/databases/embl/doc/usrman.txt
# Some features and qualifiers are ignored, where there is no appropriate
#   analogue available in EMBL.
# To run in batch mode via bash:
#   $ for i in `ls`; do /path/to/gff2embl.rb -i $i; done

require 'optparse'
require 'csv'

# $3 in the GFF. These go the entire length of each contig, and there doesn't
#   seem to be an equivalent feature in EMBL.
$features_to_skip = %w(apicoplast_chromosome chromosome supercontig)
# $3 in the GFF. These features are labelled identically in both formats.
$features_in_both_formats = %w(CDS exon gene mRNA rRNA tRNA)
# Within $9 in the GFF. These are features that EMBL doesn't seem to support,
#   and are unimportant (IMO). Some are redundant anyway, with similar data
#   contained elsewhere. For (some?)gene features, ID == Name == locus_tag ==
#   web_id
$qualifiers_to_skip = %w(web_id Alias Ontology_term size Name locus_tag)

################################################################################
options = {}
OptionParser.new do |opts|
  opts.banner='This script converts a EuPathDB GFF file into EMBL format.
Usage:'
  opts.on_tail('-h', '--help', 'Show this message') do
    puts opts; exit
  end
  opts.on('-i', '--input GFF_FILE.gff', 'GFF_FILE.gff required as input (one '\
      'sequence per file)') do |i|
    options[:input] = i
  end
  opts.on('-o', '--output EMBL_FILE', 'Output will be written to '\
      'GFF_FILE.embl, unless this option is manually specified') do |o|
    options[:output] = o
  end
  opts.on('-c', '--complement_alt', 'Use join(complement, not '\
      'complement(join. Some programs (e.g. RATT) only accept the latter') do
    options[:complement_alt] = true
  end
  opts.on('-v', '--[no-]verbose', 'Run verbosely') do |v|
    options[:verbose] = v
  end
end.parse!

# Check -i option and argument.
raise OptionParser::MissingArgument if options[:input].nil?
raise OptionParser::InvalidArgument if !(options[:input].end_with?('.gff'))
options[:output] = "#{File.dirname(options[:input])}/"\
    "#{File.basename(options[:input], '.gff')}.embl" if options[:output].nil?
contig_name = File.basename(options[:input], '.gff')

################################################################################
### Define contig object.
class Contig
  def initialize(contig_name, sequence)
    @contig_name = contig_name
    @sequence = sequence
    @total_bp = @sequence.length
    @a_count = @sequence.count 'Aa'
    @t_count = @sequence.count 'Tt'
    @c_count = @sequence.count 'Cc'
    @g_count = @sequence.count 'Gg'
    @other_count = (@total_bp - @a_count - @t_count - @c_count - @g_count)
    @features = {}
    $features_in_both_formats.each do |feature_type|
      @features[feature_type.to_sym] = {}
    end

  end

  # @features == {feature: {product: {gene: x, name: y, db_xref: z,
  #   strand: "+/-/.", coords:[[a,b],[c,d],…]},…},…}
  def add_feature(feature, start, stop, strand, product, gene, name, db_xref)
    if @features[feature][product].nil?
      @features[feature][product] = {gene: gene, name: name, db_xref: db_xref,
          strand: strand, coords:[[start, stop]]}
    else
      if @features[feature][product][:gene] != gene ||
          @features[feature][product][:name] != name ||
          @features[feature][product][:db_xref] != db_xref ||
          @features[feature][product][:strand ] != strand
        abort("Inconsistent feature data for #{product} #{feature}")
      end
      @features[feature][product][:coords].push([start, stop])
    end
  end

  def write_embl(output_path, complement_alt)
    File.open(output_path, 'w') do |output_file|
      # Write header.
      output_file.puts("ID   #{@contig_name}; SV 1; linear; genomic DNA; "\
          "MGA; UNC; #{@total_bp} BP.
XX
FH   Key             Location/Qualifiers
FH")

    # Write features. http://www.ebi.ac.uk/ena/WebFeat/
    @features.each do |feature_type, features_by_type|
      features_by_type.each do |product, feature_info|
        # Write feature's first line.
        output_file.print "FT   #{feature_type.to_s.ljust(16)}"

        # Sort coords in ascending order.
        feature_info[:coords].collect! { |coord_pair| coord_pair.sort }
        feature_info[:coords].sort! { |x, y| x[0] <=> y[0] } # sort_by is quicker for >= 12 pairs

        # Write coords as 12..45 or join(12..78,134..202) or
        #   complement(join(2691..4571,4918..5163)). Alternatively, if
        #   complement option is set, use join(complement(4918..5163),complement(2691..4571))
        coord_output = ''
        if !complement_alt
          feature_info[:coords].each do |coord_pair|
            coord_output << "#{coord_pair.first}..#{coord_pair.last},"
          end
          coord_output.chop! # Remove trailing ','.
          if !(feature_info[:coords].length == 1)
            coord_output = "join(#{coord_output})"
          end
          if feature_info[:strand] == '-'
            coord_output = "complement(#{coord_output})"
          end
        else
          if feature_info[:strand] == '-'
            feature_info[:coords].reverse.each do |coord_pair|
              coord_output << "complement(#{coord_pair.first}..#{coord_pair.last}),"
            end
          else # + or .
            feature_info[:coords].each do |coord_pair|
              coord_output << "#{coord_pair.first}..#{coord_pair.last},"
            end
          end
          coord_output.chop! # Remove trailing ','.
          if !(feature_info[:coords].length == 1)
            coord_output = "join(#{coord_output})"
          end
        end
        output_file.puts coord_output

        # Write qualifiers.
        output_file.puts "FT                   /product=\"#{product}\""
        output_file.puts "FT                   /gene=\"#{feature_info[:gene]}\""
        output_file.puts "FT                   /standard_name=\"#{feature_info[:name]}\""
        output_file.puts "FT                   /db_xref=\"#{feature_info[:db_xref]}\"" if !feature_info[:db_xref].nil?
      end
    end

    # Write sequence. Sequences in EuPathDB GFF files are already 60 bp.
      output_file.puts('XX')
      output_file.puts("SQ   Sequence #{@total_bp} BP; #{@a_count} A;"\
          "#{@c_count} C; #{@g_count} G; #{@t_count} T; #{@other_count} other;")

      if @total_bp.to_s.length > 8
        abort('sequence is longer than eight digits')
      end

      (1..(@total_bp.to_f/60).ceil).each do |i|
        seq_line = '     '
        6.times do
          seq_line << @sequence[0..9] + ' '
          @sequence[0..9] = ''
        end

        # Write bp count index.
        if seq_line.length == 71
          seq_line << (i*60).to_s.rjust(9)
        else
          seq_line << @total_bp.to_s.rjust(9 + 71 - seq_line.length)
        end
        output_file.puts(seq_line)
      end

      output_file.puts('//')
    end
  end
end

################################################################################
### Read GFF file, and write EMBL file.
# Get fasta information.
sequence = ''
fasta_section = false
File.open(options[:input]).each do |line|
  if !fasta_section
    fasta_section = true if line.start_with? '>'
  else
    sequence << line.chomp
  end
end
contig = Contig.new(contig_name, sequence)

# Get feature information.
CSV.foreach(options[:input], :col_sep => "\t", :quote_char => "\x00") do |row|
  break if row == %w(##FASTA)
  if !row[0].start_with?('##')
    # Read and parse GFF file.
    gff = {}
    # Sequence name, should == basename of file == row[0].
    #   This is true if contigs were split with split_gff_into_contigs.sh .
    # Source, i.e. ApiDB == row[1].
    gff[:feature] = row[2] # feature type name
    if !$features_to_skip.include?(gff[:feature])
      abort("unexpected feature '#{gff[:feature]}' on line #{$.}") if !$features_in_both_formats.include?(gff[:feature])
      gff[:start] = row[3]
      gff[:stop] = row[4]
      abort("unexpected score != '.' on line #{$.}") if row[5] != '.' # score
      gff[:strand] = row[6]
      abort "strand field does not contain +/-/. on line #{$.}" if !%w(+ - .).
          include? gff[:strand]
      # gff[:frame] = row[7] # EMBL format doesn't support this.
      gff_col_9 = {}
      row[8].split(';').each do |tag_pair|
        tag_pair_split = tag_pair.split('=')
        gff_col_9[tag_pair_split[0]] = tag_pair_split[1] if !$qualifiers_to_skip.include? tag_pair_split[0]
      end

      # Add qualifiers to the correct keys.
      gff_col_9.each do |k, v|
        case k
          when 'ID'
            gff[:product] = v # Specific product (e.g. if mRNA -> transcript).
          when 'Parent' # If CDS or exon, this is of the form 'rna_GENE-1'.
            gff[:gene] = v.sub(/^rna_(.*)-1$/, '\1')
          when 'description' # Full gene name (if it exists), otherwise garbage.
            gff[:name] = v
          when 'Dbxref' # Doesn't always exist.
            gff[:db_xref] = v
          else
            abort("unexpected qualifier #{k} on line #{$.}")
        end
        # Genes don't have parents. Use ID.
        gff[:gene] = gff_col_9['ID'] if gff[:feature] == 'gene'
      end

      # Write features to contig object.
      contig.add_feature(gff[:feature].to_sym, gff[:start], gff[:stop],
          gff[:strand], gff[:product].to_sym, gff[:gene].to_sym,
          gff[:name].to_sym, (gff[:db_xref].to_sym unless gff[:db_xref].nil?))
    end
  end
end

# Write EMBL file.
contig.write_embl(options[:output], options[:complement_alt])