#!/usr/bin/env ruby
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Given a gff file downloaded from EuPathDB, extract gene IDs and GO accession
#   number. These accession numbers do not appear to be available for the normal
#   bulk download function.

require 'csv'

abort('USAGE: single argument should be path to input file.') if ARGV.length != 1

CSV.foreach(ARGV[0], :col_sep => "\t", :quote_char => "\x00") do |row|
  id = nil
  go = nil
  if row[8]
    row[8].split(';').each do |item|
      if item =~ /^Parent=(.*)$/
        id = $1
      elsif item =~ /^Ontology_term=(.*)$/
        go = $1
      end
    end
  end
  if go
    abort("No associated gene ID for line:\n#{row}") if id.nil?
    puts "#{id}\t#{go}"
  end
end