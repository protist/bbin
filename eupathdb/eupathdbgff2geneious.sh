#!/usr/bin/env sh
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Modify a gff exported from eupathdb into a format that Geneious is happy with
# Reads gff file from argument, or stdin (in that priority)
# Writes to new file automatically, unless piped out or no argument

set -o noclobber
set -e

# If argument exists, read from file, else stdin
if [ $# -eq 0 ]; then
  input="$(</dev/stdin)"
else
  input="$(<"$1")"
fi

MY_TMPDIR=$(mktemp -d)

# Delete until `##gff-version 3`
<<<"$input" awk 'BEGIN {discard = 1} ; /^##gff-version 3$/ {discard = 0} ; discard == 0' |
  # Convert `##sequence-region` line, converting : and .. to a space
  sed -r 's/^(##sequence-region [^:]*):([0-9]*)\.\.([0-9]*)$/\1 \2 \3/' |
  # Replace feature's ID number with parent-gene ID number, so Geneious joins the exons
  sed -r 's/(ID=[^\.]*.)[0-9]*;Parent=gene.([0-9]*)/\1\2;Parent=gene.\2/' |
  # Bug in eupathDB: field 8 (frame) is 1 (second base is start of codon), but should be 0
  # There are also some rows with field 8 = ".", but this is also non-standard
  awk -v 'FS=\t' -v 'OFS=\t' '$8 != "" {$8 = 0} ; {print}' > "$MY_TMPDIR/default.order"

# Bug in Geneious 5.5.9: reverse strand CDS/exons must be ordered from highest index to lowest, otherwise frame starts from 3' exon, and goes backwards. This is a quick fix and doesn't check order. If eupathdb changes its gff format, then this will fail in the future.
cd "$MY_TMPDIR"
<default.order awk \
 'BEGIN {fasta = 0}
  /^>/ {fasta = 1}
  {if ($7 == "-")
     {print > "rev"}
   else if (fasta == 1)
     {print > "fasta"}
   else
     {print > "header_and_fwd"}
  }'

# Store parts in a variable
output="$(cat header_and_fwd; tac rev; cat fasta)"

# Write to file or stdout? Is there a | or > ?
if [ -t 1 ] ; then
  # Out is connected to a terminal
  if [ $# -eq 0 ]; then
    # No argument to derive a filename from, print to stdout
    printf '%s\n' "$output"
  else
    # Argument provided, input from file -> write to a file
    cd - > /dev/null
    printf '%s\n' "$output" > "${1%.gff3}.fix_for_geneious.gff3"
  fi
else
  # Out is *not* connected to a terminal (i.e. there is | or >), print to stdout regardless of argument
  printf '%s\n' "$output"
fi

# Clean up
rm -r "$MY_TMPDIR"

# Another bug in Geneious in some version required the order of the bp index reversed. This is not requred in Geneious 5.5.9, otherwise the following code might be useful.
# awk -v 'FS=\t' -v 'OFS=\t' '{if ($7 == "-") {t = $4; $4 = $5; $5 = t}; print}'
