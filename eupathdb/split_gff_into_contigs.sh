#!/usr/bin/env bash
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Reads a eupathdb gff file (as specified in the argument), then splits it into
#   single files for each contig/chromosome. Output files are written into the
#   specified directory. N.B. only the sequence for the full contig is written.
#   i.e. the sequences for the genes, CDS, etc. are ignored.
#
# Expected format of the input file:
#   <initial header>
#   ##sequence-region	TGME49_chrII	1	2347032
#   <repeated for each contig>
#   TGME49_chrII	ApiDB	chromosome	1	2347032	.	+	.	ID=TGME49_chrII;Name=TGME49_chrII;description=TGME49_chrII;size=2347032;web_id=TGME49_chrII;molecule_type=dsDNA;organism_name=Toxoplasma gondii;translation_table=1;topology=linear;localization=nuclear;Dbxref=ApiDB_ToxoDB:TGME49_chrII,taxon:508771
#   <features repeated>
#   ##FASTA
#   <fasta section>
#   <EOF>

usage="USAGE:\n  ${0} input.gff output/directory"

if [ $# -ne 2 ]
then
  echo -e "${usage}"
  exit 1
fi

# Don't overwrite existing files. Safer, but possibly annoying, so comment out if necessary.
set -o noclobber

# Get list of contigs in the file, based on ##sequence-region header.
contigs=$(sed -nr 's/^##sequence-region\t([^\t]*)\t.*$/\1/p' "${1}")

# Output progress.
echo "Preparing to write $(echo "${contigs}" | wc -l) contigs."

# Find number of lines in the initial header, to copy to all files.
first_non_header=$(grep -Pnm 1 '^##sequence-region\t' "${1}" | cut -f1 -d:)

# Find fasta section. Only grep features before this. For TgME49, 1.6 of 1.7 million lines are fasta.
fasta_start=$(grep -nm 1 '^##FASTA$' "${1}" | cut -f1 -d:)
total_lines=$(wc -l "${1}" | cut -f1 -d' ')

# Create output directory.
mkdir -p "${2}"

# Write contigs to output directory.
i=1
for contig in ${contigs}; do
  echo -en "\rWriting contig ${i}."
  ((i++))
  output_file="${2}/${contig}.gff"
  head -n$((first_non_header - 1)) "${1}" > "${output_file}"
  grep -Pm 1 "^##sequence-region\t${contig}\t" "${1}" >> "${output_file}"
  head -n$((fasta_start - 1)) "${1}" | grep -P "^${contig}\t" >> "${output_file}"
  # Write sequence information
  echo '##FASTA' >> "${output_file}"
  seq_start=$(grep -nm 1 "^>apidb|${contig}$" "${1}" | cut -f1 -d:)
  # tail often terminates with "broken pipe" error, when grep finishes and pipe closes prematurely. Hence, 2>/dev/null
  seq_length=$(tail -$((total_lines - seq_start)) "${1}" 2>/dev/null| grep -nm 1 "^>" | cut -f1 -d:)
  if [[ -z $seq_length ]]; then
    tail -$((total_lines - seq_start + 1)) "${1}" >> "${output_file}"
  else
    tail -$((total_lines - seq_start + 1)) "${1}" 2>/dev/null | head -${seq_length} >> "${output_file}" # Similarly here.
  fi
done
