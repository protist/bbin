#!/usr/bin/env bash
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Before use, check that overlaps are consistent with previous version, using
#   split_transcripts_as_per_reference.rb

# Check arguments.
usage="USAGE:\n  ${0} input.gff list_of_genes_and_cds_to_remove.txt\n  This script also strips sequence information."

if [ $# -ne 2 ]
then
  echo -e "${usage}" >&2
  exit 1
fi

################################################################################
### Parse deletions to do.
# List of genes to delete, along with expected annotation.
genes_to_check_and_delete=$(sed -n '/^# genes_to_check_and_delete$/,/^$/p' "${2}"| head -n -1| tail -n +2)

# List of genes to delete without checking annotation. These overlap unequivocally.
genes_to_delete=$(sed -n '/^# genes_to_delete$/,/^$/p' "${2}"| head -n -1| tail -n +2)

# List of exons to delete without checking annotation.
cds_to_delete=$(sed -n '/^# cds_to_delete$/,/^$/p' "${2}"| head -n -1| tail -n +2)

################################################################################
### Script proper

# Advance fifo counter and remove offending line from file.
remove_pattern () {
  nextfifocount=$(($fifocount + 1 ))
  mkfifo ${fifodir}${nextfifocount}
  grep ${1} ${fifodir}${fifocount} -v > ${fifodir}${nextfifocount} &
  fifocount=$nextfifocount
}

# Set up named pipes.
fifodir='/tmp/tempfifodir/'
rm $fifodir -rf &> /dev/null
mkdir $fifodir
fifocount=0
mkfifo ${fifodir}${fifocount}

# Remove fasta section. I haven't bothered to implement removal of specific
#   sequences, and besides, removing this section will increase efficiency.
#   For TgME49 v9.0, 1.7 of 1.8 million lines are fasta.
fasta_start=$(grep -nm 1 '^##FASTA$' "${1}" | cut -f1 -d:)
head -n $(($fasta_start - 1)) "${1}" > ${fifodir}${fifocount} &

# Check that genes' annotation haven't changed. If not, then delete.
while read -r line; do
  gene=$(echo $line | cut -d " " -f 1)
  annotation=$(echo $line | cut -d " " -f 2)
  if $(grep $gene "${1}" | awk '$3 == "gene"' | grep "description=${annotation};" -q)
  then
    remove_pattern $gene
  else
    echo "ERROR: ${gene} has unexpected annotation." >&2
    exit
  fi
done <<< "$genes_to_check_and_delete"

# Delete these genes without checking annotation.
while read -r line; do
  if [[ $(grep $line "${1}" | awk '$3 == "gene"') != '' ]]
  then
    remove_pattern $line
  else
    echo "ERROR: ${line} gene cannot be found." >&2
    exit
  fi
done <<< "$genes_to_delete"

# Delete these exons without checking annotation.
while read -r line; do
  if [[ $(grep $line "${1}" | awk '$3 == "CDS"') != '' ]]
  then
    remove_pattern $line
  else
    echo "ERROR: ${line} is not CDS." >&2
    exit
  fi
done <<< "$cds_to_delete"

# Write to stdout.
cat "${fifodir}${fifocount}"