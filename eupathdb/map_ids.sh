#!/usr/bin/env bash
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Maps old gene ids in primary_file with (first occurrence of) proper id from reference_file.gff.
# Expects first field of primary_file to contain old gene, and adds new id as second field.
# N.B. this script is not tested.

# Check arguments.
usage="USAGE:\n  ${0} primary_file reference_file.gff"

if [ $# -ne 2 ]
then
  echo -e "${usage}" >&2
  exit 1
fi

# Don't overwrite existing files. Safer, but possibly annoying, so comment out if necessary.
set -o noclobber

# N.B. doesn't deal well with header lines. Could make the newgeneidline match more specific (i.e. match whole of string).
while read -r line; do # -r (raw) so that backslashes aren't interpreted
  oldgeneid=$(echo $line | awk '{print $1}')
  newgeneidline=$(egrep -m 1 "${oldgeneid}(,|$)" "${2}") # What if there are multiple matches?

  newgeneid=$(echo $newgeneidline | sed -r 's/^.*ID=([^;]*);.*$/\1/')
  
  # N.B. $line *must* be double-quoted below, otherwise word-splitting will occur, changing whitespace into a single space.
  echo "${line}" | sed -r 's/^([^\t]*\t)(.*)$/\1'"${newgeneid}"'\t\2/'

done <"${1}"