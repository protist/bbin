#!/usr/bin/env bash
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# remove newlines from the (Linux) clipboard, then reverse complement DNA
#   sequence. Sequence must only contain ATCGNatcgn.

SEQUENCE=`xclip -o -selection clipboard | tr '\n\r\t' ' '| tr -d ' '`

if [ `echo $SEQUENCE| egrep -c '[^CTAGctagNn]'` == 1 ]; then
  echo 'Error: sequence contains a character that is not ATCGN.'
else
  echo $SEQUENCE|tr '[ATCGatcg]' '[TAGCtagc]'|rev|xclip -selection clipboard
fi
