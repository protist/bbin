#!/usr/bin/env bash
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Given a list of genes and a fasta file, create a fasta file containing the
#   sequences for the list of genes.

usage="USAGE:\n  ${0} list_of_genes.txt sequences.fa"

if [ $# -ne 2 ]
then
  echo -e "${usage}" >&2
  exit 1
fi

while read -r line; do # -r (raw) so that backslashes aren't interpreted
  matches=$(grep -n "^>${line}$" "${2}")

  if [[ $? -ne 0 ]]; then
    echo "ERROR: ${line} not found" >&2
    exit 1
  fi

  if [[ $(echo "{$matches}" | wc -l) -gt 1 ]]; then
    echo "ERROR: ${line} has multiple matches" >&2; exit 1
  fi

  idpos=$(echo "${matches}" | cut -f1 -d:)
  nextidpos=$(tail -n +$(($idpos + 1)) "${2}" | grep -nm 1 '^>' | cut -f1 -d:)

  if [[ -z $nextidpos ]]; then
    tail -n +$idpos "${2}"
  else
    tail -n +$idpos "${2}" | head -n $nextidpos
  fi

done <"${1}"
