bbin
====
Bioinformatics-related bin. A collection of scripts with a bioinformatics focus.

find_low_read_coverage.rb
-------------------------
Input a pileup file (from samtools mpileup), and create a gff file with features
where coverage is below a certain read-depth threshold. This might be useful as
a mask file for cufflinks, in order to prevent it from incorrectly joining
adjacent genes in organisms with compact genomes and short intergenic regions.
This strategy is not necessarily the best option, as cufflinks doesn't strictly
ignore reads in these regions, but only "reads that could have come from [these]
transcripts". Hence, there is an option in this script to increase the overhang
of the features, but a better solution would be to process the gff file
post-cufflinks (e.g. with [split_transcripts]
(https://gitlab.com/protist/split_transcripts) or similar).

rmnl_from_sequences_in_fasta_file.rb
------------------------------------
Removes new-line characters from sequence-containing lines of a fasta file (i.e.
those that don't start with ">"). Thus the output will be a fasta file with the
sequences on a single line. This might be useful for quick and dirty sorting
(e.g. with sort from coreutils).
