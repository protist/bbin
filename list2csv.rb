#!/usr/bin/env ruby
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'optparse'
# Set default options.
options = {separator:"\t"}
OptionParser.new do |opts|
  opts.banner='Create a csv from a list.
Usage:'
  opts.on_tail('-h', '--help', 'Show this message') do
    puts opts; exit
  end
  # complex boolean flag
  opts.on('-r', '--rows NUMBER_OF_ROWS', 'Number of rows in output csv') do |r|
    options[:rows] = r
  end
  # mandatory argument
  opts.on('-s', '--separator OUTPUT_SEPARATOR',
          'Separator in output csv (default is tab)') do |s|
    options[:separator] = s
  end
end.parse!

# Mandatory "options".
raise OptionParser::MissingArgument if options[:rows].nil?

# Allow input from stdin or file by path.
data = []
ARGF.read.each_line do |line|
  data << line.chomp
end

# Create csv.
options[:rows].to_i.times do |csv_row_index|
  output_line = ''
  data.each_with_index do |field, list_row_index|
    output_line << field + options[:separator] if list_row_index % options[:rows].to_i == csv_row_index
  end
  puts output_line.chomp(options[:separator]) # remove trailing separator
end