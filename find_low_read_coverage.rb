#!/usr/bin/env ruby
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Arguments: pileup file, output file, min_depth, max_gap, extension_length
#   What are good default values?
#     min_depth = 5? max_gap = 60? extension_length = 30?
# Input: tab-delimited pileup file (from samtools mpileup)
#   chromosome name, coordinate, reference base, the number of reads covering
#     the site, read bases, and base qualities
#   e.g. TGGT1_chrII  1  N  8 ^$C^$C^$C^$C^$C^$C^$C^$C  @@CC+@C@
#   This should be ordered by chromosome, then by coordinates.
# This script will extract coordinates with number of reads < min_depth. If the
#   coordinate is within max_gap of the last line, then join them into a single
#   transcript. Else, start a new exon. This script also extends transcripts on
#   both sides by extension_length, which is necessary for cufflinks to properly
#   exclude the defined region with its -M argument.
# Output: gff file ready for cufflinks.
# 
# N.B. pileup files do not list base positions where there are zero reads.
#   This is not a problem if the mask file will be sent to cufflinks, as you can
#   specify not to join transcripts across gaps with --overlap-radius

if ARGV.length != 5
  abort("USAGE: arguments are position sensitive and as follows.\n  /path/to/input.pileup /path/to/output.gff min_depth max_gap extension_length")
end

input_path = ARGV[0]
output_path = ARGV[1]
min_depth = ARGV[2].to_i
max_gap = ARGV[3].to_i
extension_length = ARGV[4].to_i

if max_gap < (extension_length * 2)
  abort('ERROR: this script requires max_gap >= (extension_length * 2)')
end

# Iterate over lines of the pileup file. If depth < min_depth, then store
# coordinates in a hash, where the key is the chromosome name and value is an
# array containing the coordinates.
coordinates_by_chromosome = Hash.new
puts "#{Time.new}: filtering file by depth"

# Alternatively, could use CSV.foreach(input_path, :col_sep => "\t") do |line|, but can't get it to work.
File.open(input_path).each do |line|
  # these are the parts of each line that we need
  splitline = line.split "\t"
  input_chromosome = splitline[0]
  input_coordinate = splitline[1].to_i
  input_depth = splitline[3].to_i
    
  # Only create new chromosome array if it's non-empty.
  if input_depth < min_depth
    # If this is the first occurrence of this chromosome, then create an array for it.
    coordinates_by_chromosome[input_chromosome] ||= Array.new
    # Add the coordinate from this line of the pileup file.
    coordinates_by_chromosome[input_chromosome].push(input_coordinate)
  end
end

# Return lines for GFF3. Hard-coded for speed.
# GFF3 format: TGGT1_chrIb	.	exon	429783	429783	.	.	.	attributes;
# # N.B. it's possible for end_coord to be beyond the last real coordinate, but I don't think it matters.
#   I've fixed negative start_coord to suppress warnings from cufflinks.
def gff_lines(seqid, start_coord, end_coord, incrementor)
  if start_coord < 0 then start_coord = 1 end
  "#{seqid}\t.\ttranscript\t#{start_coord}\t#{end_coord}\t.\t.\t.\tID=transcript_#{incrementor}
  #{seqid}\t.\texon\t#{start_coord}\t#{end_coord}\t.\t.\t.\tParent=transcript_#{incrementor}"
end

# Create gene models directly into GFF3 file.
transcript_id = 0
File.open(output_path, 'w') do |f|
  # Write header.
  f.puts '##gff-version 3'
  # Iterate through chromosomes.
  coordinates_by_chromosome.each do |chromosome, coordinates|
    output_chr = chromosome
    # Iterate through coordinates within chromosomes.
    # First position.
    puts "#{Time.new}: Creating exon models for #{output_chr}..."
    five_prime_coord = coordinates[0]
    current_output_coord = coordinates[0]
    # Iterate through second position to penultimate position.
    (1...(coordinates.length - 1)).each do |coord_index|
      prev_output_coord = current_output_coord
      current_output_coord = coordinates[coord_index]
      if prev_output_coord + max_gap + 1 < current_output_coord # i.e. no overlap, hence we should close the exon. 
        transcript_id += 1
        f.puts gff_lines(output_chr, (five_prime_coord - extension_length), (prev_output_coord + extension_length), transcript_id)
        five_prime_coord = current_output_coord
      end
    end
    # Last position: close the exon.
    transcript_id += 1
    f.puts gff_lines(output_chr, (five_prime_coord - extension_length), (coordinates.last + extension_length), transcript_id)
    puts "#{Time.new}: #{output_chr} completed."
  end
  puts "#{Time.new}: Analysis complete"
end
