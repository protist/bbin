#!/usr/bin/env ruby
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This script removes newlines from the sequence section of fasta files (i.e.
#   lines that don't begin with >).

require 'optparse'
options = {}
OptionParser.new do |opts|
  opts.banner='This script reads in a fasta file, from a file if specified, '\
              'or from stdin if not, and removes the new-line characters from '\
              'sequence-containing lines (i.e. those that don\'t start with >'\
              '\nUsage:'
  opts.on_tail('-h', '--help', 'Show this message') do
    puts opts; exit
  end
  opts.on('-o', '--output FASTA_FILE',
          'Output to stdout, unless file is specified here.') do |o|
    options[:output_path] = o
  end
end.parse!

if options[:output_path]
  $stdout = File.open(options[:output_path], 'w')
end

prev_line = nil
ARGF.each do |line|
  if !prev_line
    print line.chomp
  else
    if !(prev_line.start_with?('>')) && !(line.start_with?('>'))
      print line.chomp
    else
      print "\n" + line.chomp
    end
  end
  $stdout.flush
  prev_line = line
end
print "\n"